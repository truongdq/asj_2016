#!/bin/bash
if [ ! -d output ];
then
    mkdir output
fi

# Sometime platex does not work correctly when the bibtex
# changed -> we need to run it several times.

platex -shell-escape -output-directory="output" -synctex=1 "$1" && \
dvipdfmx -V 4 "output/`basename "$1" .tex`"
bibtex output/main
platex -shell-escape -output-directory="output" -synctex=1 "$1" && \
dvipdfmx -V 4 "output/`basename "$1" .tex`"
platex -shell-escape -output-directory="output" -synctex=1 "$1" && \
dvipdfmx -V 4 "output/`basename "$1" .tex`"
platex -shell-escape -output-directory="output" -synctex=1 "$1" && \
dvipdfmx -V 4 "output/`basename "$1" .tex`"
